## Description

This stack was created by me from scratch using recommandations explained in the great documentation by nestJS.
I also used some tips given by people answers on stackoverflow.
To finish I tried to understand and get all the best ideas or practices from 3 different projects on Github + my own experience.

I didn't add useless things, I tried to keep this project the most complete and simple possible.

If you have others recommandations / fixes to report. Don't hesitate to realize a PR on this repository.

## Installation

```bash
$ npm install

To run the DB, I use docker with mongoDB
Please install docker and docker-compose on your computer before to continue.

After that, you just have to run the following command: `docker-compose up -d`

It will run mongoDB instance on you computer using a container. 

NB: You can disable the mongoDB container using `docker-compose down`.
```


## Swagger
Swagger documentation will be available on route:
```bash
http://localhost:3000/api

Tips: Before to use DELETE, GET endpoints. Please create using POST endpoint otherwise you will only have {} answers because your mongoDB will be empty.
```

## Documentation

```bash
# unit tests
$ npm run serve-doc

Then go to: http://localhost:8080/

You will find all documentation about architecture with differents graphs / draws which explain how the project is built.
You will have all the documentation on how the API works also.
```

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Test

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```
