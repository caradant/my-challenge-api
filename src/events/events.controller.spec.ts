import { Test, TestingModule } from '@nestjs/testing';
import { CommonModule } from '../common/common.module';
import { EventDTO } from './dto';
import { EventsBizService } from './events.biz.service';
import { EventsController } from './events.controller';

// NOTE: For me something could be useful here. It's to use "mother classes". To avoid the declaration of many DTO object in each test
// I could export objects from a Mother class and just import them after in test when needed.
describe('EventsController Unit Tests', () => {
  let eventsController: EventsController;
  let spyEventsBizService: EventsBizService;

  beforeAll(async () => {
    const EventsBizServiceProvider = {
      provide: EventsBizService,
      useFactory: () => ({
        findAll: jest.fn(() => [
          {
            userId: '1123456789',
            userEmail: 'email@airdot.fr',
            userConsent: [],
            creation: new Date('2022-10-10'),
          },
        ]),
        findById: jest.fn(() => ({
          userId: '1123456789',
          userEmail: 'email@airdot.fr',
          userConsent: [],
          creation: new Date('2022-10-10'),
        })),
        findByUserEmailEvent: jest.fn(() => ({
          userId: '1123456789',
          userEmail: 'email@airdot.fr',
          userConsent: [],
          creation: new Date('2022-10-10'),
        })),
        create: jest.fn(() => ({
          userId: '1123456789',
          userEmail: 'email@airdot.fr',
          userConsent: [],
          creation: new Date('2022-10-10'),
        })),
      }),
    };

    const app: TestingModule = await Test.createTestingModule({
      controllers: [EventsController],
      providers: [EventsBizService, EventsBizServiceProvider],
    }).compile();

    eventsController = app.get<EventsController>(EventsController);
    spyEventsBizService = app.get<EventsBizService>(EventsBizService);
  });

  it('calling findAll user method', () => {
    expect(eventsController.findAll()).not.toEqual(null);
    expect(spyEventsBizService.findAll).toHaveBeenCalled();
  });

  it('calling_findById user method', () => {
    expect(eventsController.findById({ id: 'id' })).not.toEqual(null);
    expect(spyEventsBizService.findById).toHaveBeenCalled();
    expect(spyEventsBizService.findById).toHaveBeenCalledWith('id');
  });

  it('calling findByUserEmailEvent user method', () => {
    expect(
      eventsController.findByUserEmailEvent({ userEmail: 'email' }),
    ).not.toEqual(null);
    expect(spyEventsBizService.findByUserEmailEvent).toHaveBeenCalled();
    expect(spyEventsBizService.findByUserEmailEvent).toHaveBeenCalledWith(
      'email',
    );
  });

  it('calling create event method', () => {
    const eventDTO: EventDTO = {
      userId: 'thisIsUserId',
      userEmail: 'email1@france.fr',
      userConsent: [],
      creation: new Date('2011-11-11'),
    };

    expect(eventsController.create(eventDTO)).not.toEqual(null);
    expect(spyEventsBizService.create).toHaveBeenCalled();
    expect(spyEventsBizService.create).toHaveBeenCalledWith(eventDTO);
  });
});
