export interface JwtSign {
  access_token: string;
}

export interface JwtPayload {
  sub: string;
  username: string;
}

export interface Payload {
  userId: string;
  username: string;
}
