import { Injectable } from '@nestjs/common';

// This should be a real class where data come from mongoDB with real information on saved users registered.
// 'bcrypt' lib could be used in DB also to encrypt / decrypt the password.
export type User = { username: string; userId: number; password?: string };

@Injectable()
export class FakeUsersService {
  private readonly users = [
    {
      userId: 1,
      username: 'Kevin',
      password: 'root',
    },
    {
      userId: 2,
      username: 'Anthony',
      password: 'admin',
    },
  ];

  async findOne(username: string): Promise<User | undefined> {
    return this.users.find((user) => user.username === username);
  }
}
