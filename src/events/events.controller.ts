import {
  Body,
  Controller,
  Get,
  Header,
  HttpCode,
  HttpStatus,
  NotFoundException,
  Param,
  Post,
  UseGuards,
  Logger,
} from '@nestjs/common';
import {
  ApiOkResponse,
  ApiUnauthorizedResponse,
  ApiBearerAuth,
  ApiInternalServerErrorResponse,
  ApiNotFoundResponse,
  ApiBody,
  ApiParam,
  ApiUnprocessableEntityResponse,
} from '@nestjs/swagger';
import { JwtAuthGuard } from '../auth';
import { EventDTO } from './dto/event.dto';
import { EventsBizService } from './events.biz.service';
import { Event } from './schemas/event.schema';

@Controller('events')
export class EventsController {
  private readonly logger = new Logger(EventsBizService.name);

  constructor(private eventsBizService: EventsBizService) {}

  @ApiOkResponse({
    type: Array,
    description: '200. Success. Returns all events',
  })
  @ApiUnauthorizedResponse({
    type: String,
    description: '401. UnauthorizedException.',
  })
  @ApiInternalServerErrorResponse({
    schema: {
      type: 'object',
      example: {
        message: 'string',
        details: {},
      },
    },
    description: '500. InternalServerError',
  })
  @ApiUnprocessableEntityResponse({
    schema: {
      type: 'object',
      example: {
        statusCode: 'number',
        message: [],
        error: 'string',
      },
    },
    description: '422. UnprocessableEntityException',
  })
  @ApiBearerAuth()
  @Get()
  @UseGuards(JwtAuthGuard)
  public async findAll(): Promise<Event[]> {
    this.logger.debug('[CONTROLLER] Get All Events');
    return this.eventsBizService.findAll();
  }

  @ApiOkResponse({
    type: Event,
    description: '200. Success. Returns an event found by id',
  })
  @ApiNotFoundResponse({
    description: '404. NotFoundException. Event was not found',
  })
  @ApiUnauthorizedResponse({
    type: String,
    description: '401. UnauthorizedException.',
  })
  @ApiInternalServerErrorResponse({
    schema: {
      type: 'object',
      example: {
        message: 'string',
        details: {},
      },
    },
    description: '500. InternalServerError',
  })
  @ApiUnprocessableEntityResponse({
    schema: {
      type: 'object',
      example: {
        statusCode: 'number',
        message: [],
        error: 'string',
      },
    },
    description: '422. UnprocessableEntityException',
  })
  @ApiParam({ name: 'id', type: String })
  @ApiBearerAuth()
  @Get(':id')
  @UseGuards(JwtAuthGuard)
  public async findById(@Param() param): Promise<Event> {
    const eventFound = await this.eventsBizService.findById(param.id);
    if (!eventFound) {
      throw new NotFoundException('The event does not exist');
    }

    this.logger.debug(`[CONTROLLER] Get Event By Id ${param.id}`);
    return this.eventsBizService.findById(param.id);
  }

  @ApiOkResponse({
    type: Event,
    description: '200. Success. Returns an event found by userEmail',
  })
  @ApiNotFoundResponse({
    description: '404. NotFoundException. Event was not found',
  })
  @ApiUnauthorizedResponse({
    type: String,
    description: '401. UnauthorizedException.',
  })
  @ApiInternalServerErrorResponse({
    schema: {
      type: 'object',
      example: {
        message: 'string',
        details: {},
      },
    },
    description: '500. InternalServerError',
  })
  @ApiUnprocessableEntityResponse({
    schema: {
      type: 'object',
      example: {
        statusCode: 'number',
        message: [],
        error: 'string',
      },
    },
    description: '422. UnprocessableEntityException',
  })
  @ApiParam({ name: 'userEmail', type: String })
  @ApiBearerAuth()
  @Get('userEmail/:userEmail')
  @UseGuards(JwtAuthGuard)
  public async findByUserEmailEvent(@Param() param): Promise<Event> {
    this.logger.debug(`[CONTROLLER] Get Event By UserEmail ${param.userEmail}`);
    const eventFound = await this.eventsBizService.findByUserEmailEvent(
      param.userEmail,
    );
    if (!eventFound) {
      throw new NotFoundException(
        'The event does not exist with this emailUser',
      );
    }
    return this.eventsBizService.findByUserEmailEvent(param.userEmail);
  }

  @ApiBody({
    schema: {
      type: 'object',
      example: {
        userId: '62546434d2d543221b03194',
        userEmail: 'toto1@toto.fr',
        userConsent: [
          { id: 'sms', enabled: true },
          { id: 'email', enabled: true },
        ],
        creation: '2018-11-21T06:20:32.232Z',
      },
    },
  })
  @ApiOkResponse({
    type: Event,
    description: '200. Success. Create an event',
  })
  @ApiUnauthorizedResponse({
    type: String,
    description: '401. UnauthorizedException.',
  })
  @ApiInternalServerErrorResponse({
    schema: {
      type: 'object',
      example: {
        message: 'string',
        details: {},
      },
    },
    description: '500. InternalServerError',
  })
  @ApiNotFoundResponse({
    description:
      '404. NotFoundException. UserId not found in database. This event will not be saved',
  })
  @ApiUnprocessableEntityResponse({
    schema: {
      type: 'object',
      example: {
        statusCode: 'number',
        message: [],
        error: 'string',
      },
    },
    description: '422. UnprocessableEntityException',
  })
  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard)
  @Header('Cache-Control', 'none')
  @Post()
  @HttpCode(HttpStatus.OK)
  public async create(@Body() eventDTO: EventDTO): Promise<Event> {
    this.logger.debug(
      `[CONTROLLER] Create Event for the user : ${eventDTO.userEmail}`,
    );
    return this.eventsBizService.create(eventDTO);
  }
}
