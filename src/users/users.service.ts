import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { UserDTO } from './dto/user.dto';
import { User } from './schemas/user.schema';

@Injectable()
export class UsersService {
  constructor(@InjectModel('User') private readonly userModel: Model<User>) {}

  public async findAll(): Promise<User[]> {
    return this.userModel.find().exec();
  }

  public async findById(id: string): Promise<User | null> {
    return this.userModel.findOne({ _id: id });
  }

  public async findByEmail(email: string): Promise<User | null> {
    return this.userModel.findOne({ email });
  }

  public async createOrUpdate(user: UserDTO): Promise<User | null> {
    const userFound = await this.userModel.findOne({ email: user.email });
    return userFound
      ? this.userModel.findByIdAndUpdate({ _id: userFound.id }, user)
      : this.userModel.create(user);
  }

  public async deleteById(id: string): Promise<User | null> {
    return this.userModel.findByIdAndRemove({ _id: id });
  }
}
