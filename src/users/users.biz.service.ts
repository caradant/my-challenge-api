import { Injectable, Logger } from '@nestjs/common';
import mongoose from 'mongoose';
import { EventsService } from '../events/events.service';
import { EventDTO } from './../events/dto/event.dto';
import { UserDTO } from './dto/user.dto';
import { User } from './schemas/user.schema';
import { UsersService } from './users.service';

@Injectable()
export class UsersBizService {
  // NOTE: Declare the logger like that instead of using
  // the static logger class is useful to add a custom logger
  // in the app.module or even to display in logger like here
  // the class name where the logger is called;
  private readonly logger = new Logger(UsersBizService.name);

  constructor(
    private usersService: UsersService,
    private eventsService: EventsService,
  ) {}

  public async findAll(): Promise<User[] | null> {
    this.logger.debug('[BIZ] Get All users');
    return this.usersService.findAll();
  }

  public async findById(id: string): Promise<User | null> {
    this.logger.debug('[BIZ] Get user by id: ' + id);

    // NOTE: The following condition is to avoid an issue.
    // cf: https://syntaxfix.com/question/16085/mongoose-casterror-cast-to-objectid-failed-for-value-object-object-at-path-id
    // https://stackoverflow.com/questions/66395079/nestjs-and-mongoose-find-by-reference-object-id
    if (!mongoose.Types.ObjectId.isValid(id)) {
      return null;
    }

    return this.usersService.findById(id);
  }

  public async findByEmail(email: string): Promise<User | null> {
    this.logger.debug('[BIZ] Get user by email: : ' + email);
    return this.usersService.findByEmail(email);
  }

  public async deleteById(id: string): Promise<User | null> {
    this.logger.debug('[BIZ] Delete user by id: ' + id);
    if (!mongoose.Types.ObjectId.isValid(id)) {
      return null;
    }
    return this.usersService.deleteById(id);
  }

  public async createOrUpdate(userDTO: UserDTO): Promise<User | null> {
    this.logger.debug(
      '[BIZ] Create or Update user as email id: ' + userDTO.email,
    );

    return this.usersService
      .createOrUpdate(userDTO)
      .then((userCreatedOrUpdated) => {
        // NOTE: Use momentJS or a dedicated lib to manage more easily the date if needed
        const eventDTO: EventDTO = {
          userId: userCreatedOrUpdated.id,
          userEmail: userDTO.email,
          userConsent: userDTO.consent,
          creation: new Date(),
        };

        this.eventsService.create(eventDTO);

        return userCreatedOrUpdated;
      });
  }
}
