import {
  Controller,
  HttpCode,
  HttpStatus,
  Post,
  Request,
  UseGuards,
} from '@nestjs/common';
import {
  ApiBody,
  ApiOkResponse,
  ApiUnauthorizedResponse,
} from '@nestjs/swagger';
import { Payload } from './auth.interface';
import { AuthService } from './auth.service';
import { LocalAuthGuard } from './guards';

@Controller('auth')
export class AuthController {
  constructor(private authService: AuthService) {}

  @ApiOkResponse({
    type: String,
    description: '200. Success. Return an access token ( valid during 1360s )',
  })
  @ApiUnauthorizedResponse({
    type: String,
    description: '401. UnauthorizedException.',
  })
  @ApiBody({
    schema: {
      type: 'object',
      example: {
        username: 'Kevin',
        password: 'root',
      },
    },
  })
  @UseGuards(LocalAuthGuard)
  @Post('jwt/login')
  @HttpCode(HttpStatus.OK)
  public login(@Request() user: Payload) {
    return this.authService.login(user);
  }
}
