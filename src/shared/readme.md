This folder is empty for this challenge because I want to keep simple the exercice.

But it could be smart to think to put here all modules shareable.
In shared.module.ts. Just import all others modules ( components ) which can be use for this app and also for external apps. Put here only the real and independancies modules.

Otherwise if the module has dependancies with the app, we need to put it on 'common.module'.

Finally just import in app.module the sharedModule.

Example: Create an Event module which can manage all kind of possible events for many different applications / micro services.
In this case, the eventModule could be here.

