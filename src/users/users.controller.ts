import {
  Body,
  Controller,
  Delete,
  Get,
  Header,
  HttpCode,
  HttpStatus,
  Logger,
  NotFoundException,
  Param,
  Post,
  UseGuards,
} from '@nestjs/common';
import {
  ApiBearerAuth,
  ApiBody,
  ApiInternalServerErrorResponse,
  ApiNotFoundResponse,
  ApiOkResponse,
  ApiOperation,
  ApiParam,
  ApiUnauthorizedResponse,
  ApiUnprocessableEntityResponse,
} from '@nestjs/swagger';
import { JwtAuthGuard } from '../auth/guards/jwt-auth.guard';
import { UserDTO } from './dto/user.dto';
import { User } from './schemas/user.schema';
import { UsersBizService } from './users.biz.service';

@Controller('users')
export class UsersController {
  private readonly logger = new Logger(UsersController.name);

  constructor(private usersBizService: UsersBizService) {}

  @ApiOkResponse({
    type: String,
    description: '200. Success. Returns a Hello World  string',
  })
  @ApiInternalServerErrorResponse({
    schema: {
      type: 'object',
      example: {
        message: 'string',
        details: {},
      },
    },
    description: '500. InternalServerError',
  })
  @Get('/demo')
  @Header('Cache-Control', 'none')
  getHelloWorld(): string {
    return 'EndPoint Unprotected';
  }

  @ApiOkResponse({
    type: User,
    description: '200. Success. Returns all users',
  })
  @ApiNotFoundResponse({
    description: '404. NotFoundException. User was not found',
  })
  @ApiUnauthorizedResponse({
    type: String,
    description: '401. UnauthorizedException.',
  })
  @ApiInternalServerErrorResponse({
    schema: {
      type: 'object',
      example: {
        message: 'string',
        details: {},
      },
    },
    description: '500. InternalServerError',
  })
  @ApiUnprocessableEntityResponse({
    schema: {
      type: 'object',
      example: {
        statusCode: 'number',
        message: [],
        error: 'string',
      },
    },
    description: '422. UnprocessableEntityException',
  })
  @ApiOperation({ description: 'Get All users' })
  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard)
  @Get()
  @Header('Cache-Control', 'none')
  public async findAll(): Promise<User[]> {
    return this.usersBizService.findAll();
  }

  @ApiOkResponse({
    type: User,
    description: '200. Success. Returns an user found by id',
  })
  @ApiNotFoundResponse({
    description: '404. NotFoundException. User was not found',
  })
  @ApiUnauthorizedResponse({
    type: String,
    description: '401. UnauthorizedException.',
  })
  @ApiInternalServerErrorResponse({
    schema: {
      type: 'object',
      example: {
        message: 'string',
        details: {},
      },
    },
    description: '500. InternalServerError',
  })
  @ApiUnprocessableEntityResponse({
    schema: {
      type: 'object',
      example: {
        statusCode: 'number',
        message: [],
        error: 'string',
      },
    },
    description: '422. UnprocessableEntityException',
  })
  @ApiOperation({ description: 'Get an user using ID' })
  @ApiBearerAuth()
  @ApiParam({ name: 'id', type: String })
  @UseGuards(JwtAuthGuard)
  @Get(':id')
  @Header('Cache-Control', 'none')
  public async findById(@Param() param): Promise<User | null> {
    this.logger.debug('[CONTROLLER] Get User with id: ' + param.id);
    const userFound = await this.usersBizService.findById(param.id);
    if (!userFound) {
      throw new NotFoundException('The user does not exist');
    }
    return userFound;
  }

  @ApiOkResponse({
    type: User,
    description: '200. Success. Returns an user found by email',
  })
  @ApiNotFoundResponse({
    description: '404. NotFoundException. User was not found',
  })
  @ApiUnauthorizedResponse({
    type: String,
    description: '401. UnauthorizedException.',
  })
  @ApiInternalServerErrorResponse({
    schema: {
      type: 'object',
      example: {
        message: 'string',
        details: {},
      },
    },
    description: '500. InternalServerError',
  })
  @ApiUnprocessableEntityResponse({
    schema: {
      type: 'object',
      example: {
        statusCode: 'number',
        message: [],
        error: 'string',
      },
    },
    description: '422. UnprocessableEntityException',
  })
  @ApiOperation({ description: 'Get an user using its email' })
  @ApiBearerAuth()
  @ApiParam({ name: 'email', type: String })
  @UseGuards(JwtAuthGuard)
  @Get('/email/:email')
  @Header('Cache-Control', 'none')
  public async findByEmail(@Param() param): Promise<User> {
    this.logger.debug('[CONTROLLER] Get User with email: ' + param.email);
    const userFound = await this.usersBizService.findByEmail(param.email);
    if (!userFound) {
      throw new NotFoundException('The user does not exist');
    }
    return userFound;
  }

  // TODO: For the update, regarding the API REST convention, it's recommanded
  // to use PUT as verb HTTP. However, in the readme, are allowed only POST / GET / DELETE
  // so I will use POST for this use case.
  // It will mix the creation and update
  @ApiOkResponse({
    type: User,
    description: '200. Success. Create an user',
  })
  @ApiUnauthorizedResponse({
    type: String,
    description: '401. UnauthorizedException.',
  })
  @ApiInternalServerErrorResponse({
    schema: {
      type: 'object',
      example: {
        message: 'string',
        details: {},
      },
    },
    description: '500. InternalServerError',
  })
  @ApiBody({
    schema: {
      type: 'object',
      example: {
        email: 'toto2@example.fr',
        consent: [
          {
            type: 'email',
            enabled: false,
          },
          {
            type: 'sms',
            enabled: true,
          },
        ],
      },
    },
  })
  @ApiUnprocessableEntityResponse({
    schema: {
      type: 'object',
      example: {
        statusCode: 'number',
        message: [],
        error: 'string',
      },
    },
    description: '422. UnprocessableEntityException',
  })
  @ApiOperation({
    description:
      'Create / Update update an user depending if the email is already exist',
  })
  @ApiBearerAuth()
  @UseGuards(JwtAuthGuard)
  @Post()
  @HttpCode(HttpStatus.OK)
  @Header('Cache-Control', 'none')
  public async createOrUpdate(@Body() userDTO: UserDTO): Promise<UserDTO> {
    this.logger.debug(
      '[CONTROLLER] Create Or Update User with email: ' + userDTO.email,
    );
    return this.usersBizService.createOrUpdate(userDTO);
  }

  @ApiOkResponse({
    type: User,
    description: '200. Success. Delete an user',
  })
  @ApiNotFoundResponse({
    description: '404. NotFoundException. User was not found',
  })
  @ApiUnauthorizedResponse({
    type: String,
    description: '401. UnauthorizedException.',
  })
  @ApiInternalServerErrorResponse({
    schema: {
      type: 'object',
      example: {
        message: 'string',
        details: {},
      },
    },
    description: '500. InternalServerError',
  })
  @ApiUnprocessableEntityResponse({
    schema: {
      type: 'object',
      example: {
        statusCode: 'number',
        message: [],
        error: 'string',
      },
    },
    description: '422. UnprocessableEntityException',
  })
  @ApiOperation({
    description: 'Remove an user using ID',
  })
  @ApiBearerAuth()
  @ApiParam({ name: 'id', type: String })
  @UseGuards(JwtAuthGuard)
  @Delete(':id')
  @HttpCode(HttpStatus.OK)
  @Header('Cache-Control', 'none')
  public async delete(@Param() param): Promise<User> {
    this.logger.debug('[CONTROLLER] Delete User by ID: ' + param.id);
    const userFound = await this.usersBizService.deleteById(param.id);
    if (!userFound) {
      throw new NotFoundException('The user does not exist');
    }
    return userFound;
  }
}
