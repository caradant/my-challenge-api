import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { EventDTO } from './dto/event.dto';
import { Event } from './schemas/event.schema';

@Injectable()
export class EventsService {
  constructor(
    @InjectModel('Event') private readonly eventModel: Model<Event>,
  ) {}

  public async findAll(): Promise<Event[]> {
    return this.eventModel.find().exec();
  }

  public async findByUserEmailEvent(userEmail: string): Promise<Event> {
    return this.eventModel.findOne({ userEmail });
  }

  public async findById(id: string): Promise<Event> {
    return this.eventModel.findOne({ _id: id });
  }

  public async create(event: EventDTO): Promise<Event> {
    return new this.eventModel(event).save();
  }
}
