import { Logger } from '@nestjs/common/services/logger.service';
import { Test, TestingModule } from '@nestjs/testing';
import { UserDTO } from './dto/user.dto';
import { UsersBizService } from './users.biz.service';

// NOTE: For services in this project, I choose to mock every service and return something expected.
// For me something could be useful here. It's to use "mother classes". To avoid the declaration of many DTO object in each test
// I could export objects from a Mother class and just import them after in test when needed.
class UsersBizServiceMock {
  findAll() {
    return <UserDTO[]>[
      {
        email: 'email@airdot.fr',
        consent: [],
      },
      {
        email: 'email2@airdot.fr',
        consent: [],
      },
    ];
  }
  findById(id: string) {
    Logger.debug(id);
    return <UserDTO>{
      email: 'email@airdot.fr',
      consent: [],
    };
  }
  findByEmail(email: string) {
    Logger.debug(email);
    return <UserDTO>{
      email: 'email@airdot.fr',
      consent: [],
    };
  }
  createOrUpdate(dto: UserDTO) {
    Logger.debug(dto.email);
    return <UserDTO>{
      email: 'email3@airdot.fr',
      consent: [],
    };
  }
  deleteById(id: string) {
    Logger.debug(id);
    return <UserDTO>{
      email: 'email3@airdot.fr',
      consent: [],
    };
  }
}

describe('UsersBizService', () => {
  let usersBizService: UsersBizService;

  beforeAll(async () => {
    const UserBizServiceProvider = {
      provide: UsersBizService,
      useClass: UsersBizServiceMock,
    };

    const module: TestingModule = await Test.createTestingModule({
      providers: [UsersBizService, UserBizServiceProvider],
    }).compile();

    usersBizService = module.get<UsersBizService>(UsersBizService);
  });

  it('UsersBizService - should be defined', () => {
    expect(usersBizService).toBeDefined();
  });

  it('should call createOrUpdate method with expected params', async () => {
    const expectedUser: UserDTO = {
      email: 'email3@airdot.fr',
      consent: [],
    };
    const userCreated = await usersBizService.createOrUpdate(expectedUser);
    expect(userCreated).toEqual(expectedUser);
  });

  it('should call findOneById method with expected param', async () => {
    const userExpected: UserDTO = {
      email: 'email@airdot.fr',
      consent: [],
    };
    const userById = await usersBizService.findById('1123456789');
    expect(userById).toEqual(userExpected);
  });

  it('should call findOneByEmail method with expected params', async () => {
    const userExpected: UserDTO = {
      email: 'email@airdot.fr',
      consent: [],
    };
    const userByEmail = await usersBizService.findByEmail('email@airdot.fr');
    expect(userByEmail).toEqual(userExpected);
  });

  it('should call findAll method', async () => {
    const usersExpected: UserDTO[] = [
      {
        email: 'email@airdot.fr',
        consent: [],
      },
      {
        email: 'email2@airdot.fr',
        consent: [],
      },
    ];
    const users = await usersBizService.findAll();
    expect(users).toEqual(usersExpected);
  });

  it('should call deleteById method with expected param', async () => {
    const userExpected: UserDTO = {
      email: 'email3@airdot.fr',
      consent: [],
    };
    const userDeleteById = await usersBizService.deleteById('112345678911');
    expect(userDeleteById).toEqual(userExpected);
  });
});
