import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { ApiProperty } from '@nestjs/swagger';
import { Document } from 'mongoose';
import { Consent } from '../interfaces/users.interfaces';

// NOTE: In mongoDB, I executed the following command to add a constraint on DB
// db.users.createIndex({ email: 1 }, { unique: true });

@Schema()
export class User extends Document {
  @ApiProperty()
  @Prop({ type: String, required: true, unique: true })
  email: string;

  @ApiProperty({
    type: 'array',
    format: 'object',
    example: '{type: "sms", enabled: true, type: "email", enabled: false}',
  })
  @Prop({ type: Array })
  consent: Consent[];
}

export const UserSchema = SchemaFactory.createForClass(User);
