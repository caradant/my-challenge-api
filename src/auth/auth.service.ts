import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { FakeUsersService } from '../common';
import { JwtPayload, JwtSign, Payload } from './auth.interface';

@Injectable()
export class AuthService {
  constructor(
    private fakeUsersService: FakeUsersService,
    private jwtService: JwtService,
  ) {}

  public async validateUser(
    username: string,
    pass: string,
  ): Promise<{
    userId: number;
    username: string;
  } | null> {
    const user = await this.fakeUsersService.findOne(username);
    if (user && user.password === pass) {
      // Useful syntax to get the user without the password information
      const { password, ...result } = user; // eslint-disable-line @typescript-eslint/no-unused-vars

      return result;
    }
    return null;
  }

  public login(user: Payload): JwtSign {
    // we keep the 'sub' name attribute on userId to hold our compliance with JWT
    const payload: JwtPayload = { username: user.username, sub: user.userId };
    return {
      access_token: this.jwtService.sign(payload),
    };
  }
}
