import { Logger } from '@nestjs/common';
import { Test, TestingModule } from '@nestjs/testing';
import { UserDTO } from './dto/user.dto';
import { UsersService } from './users.service';

// NOTE: For services in this project, I choose to mock every service and return something expected.
// For me something could be useful here. It's to use "mother classes". To avoid the declaration of many DTO object in each test
// I could export objects from a Mother class and just import them after in test when needed.
class UsersServiceMock {
  findAll() {
    return <UserDTO[]>[
      {
        email: 'email@airdot.fr',
        consent: [],
      },
      {
        email: 'email2@airdot.fr',
        consent: [],
      },
    ];
  }
  findById(id: string) {
    Logger.debug(id);
    return <UserDTO>{
      email: 'email@airdot.fr',
      consent: [],
    };
  }
  findByEmail(email: string) {
    Logger.debug(email);
    return <UserDTO>{
      email: 'email@airdot.fr',
      consent: [],
    };
  }
  createOrUpdate(dto: UserDTO) {
    Logger.debug(dto.email + 'and ' + dto.consent);
    return <UserDTO>{
      email: 'email3@airdot.fr',
      consent: [],
    };
  }
  deleteById(id: string) {
    Logger.debug(id);
    return <UserDTO>{
      email: 'email3@airdot.fr',
      consent: [],
    };
  }
}
describe('UsersService', () => {
  let usersService: UsersService;

  beforeAll(async () => {
    const ApiServiceProvider = {
      provide: UsersService,
      useClass: UsersServiceMock,
    };
    const module: TestingModule = await Test.createTestingModule({
      providers: [UsersService, ApiServiceProvider],
    }).compile();

    usersService = module.get<UsersService>(UsersService);
  });

  it('UsersService - should be defined', () => {
    expect(usersService).toBeDefined();
  });

  it('should call createOrUpdate method with expected params', async () => {
    const expectedUser: UserDTO = {
      email: 'email3@airdot.fr',
      consent: [],
    };
    const userCreated = await usersService.createOrUpdate(expectedUser);
    expect(userCreated).toEqual(expectedUser);
  });

  it('should call findOneById method with expected param', async () => {
    const userExpected: UserDTO = {
      email: 'email@airdot.fr',
      consent: [],
    };
    const userById = await usersService.findById('1123456789');
    expect(userById).toEqual(userExpected);
  });

  it('should call findOneByEmail method with expected params', async () => {
    const userExpected: UserDTO = {
      email: 'email@airdot.fr',
      consent: [],
    };
    const userByEmail = await usersService.findByEmail('email@airdot.fr');
    expect(userByEmail).toEqual(userExpected);
  });

  it('should call deleteById method with expected param', async () => {
    const userExpected: UserDTO = {
      email: 'email3@airdot.fr',
      consent: [],
    };
    const userDeleteById = await usersService.deleteById('112345678911');
    expect(userDeleteById).toEqual(userExpected);
  });
});
