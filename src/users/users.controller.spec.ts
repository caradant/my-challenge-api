import { Test, TestingModule } from '@nestjs/testing';
import { CommonModule } from '../common/common.module';
import { UserDTO } from './dto/user.dto';
import { UsersBizService } from './users.biz.service';
import { UsersController } from './users.controller';

// NOTE: For me something could be useful here. It's to use "mother classes". To avoid the declaration of many DTO object in each test
// I could export objects from a Mother class and just import them after in test when needed.
describe('UsersController Unit Tests', () => {
  let usersController: UsersController;
  let spyUsersBizService: UsersBizService;

  beforeAll(async () => {
    const UsersBizServiceProvider = {
      provide: UsersBizService,
      useFactory: () => ({
        findAll: jest.fn(() => [
          {
            email: 'email3@airdot.fr',
            consent: [],
          },
        ]),
        findByEmail: jest.fn(() => ({
          email: 'email3@airdot.fr',
          consent: [],
        })),
        findById: jest.fn(() => ({
          email: 'email3@airdot.fr',
          consent: [],
        })),
        deleteById: jest.fn(() => ({
          email: 'email3@airdot.fr',
          consent: [],
        })),
        createOrUpdate: jest.fn(() => ({
          email: 'email3@airdot.fr',
          consent: [],
        })),
      }),
    };

    const app: TestingModule = await Test.createTestingModule({
      controllers: [UsersController],
      providers: [UsersBizService, UsersBizServiceProvider],
    }).compile();

    usersController = app.get<UsersController>(UsersController);
    spyUsersBizService = app.get<UsersBizService>(UsersBizService);
  });

  it('calling findAll user method', () => {
    expect(usersController.findAll()).not.toEqual(null);
    expect(spyUsersBizService.findAll).toHaveBeenCalled();
  });

  it('calling findById user method', () => {
    expect(usersController.findById({ id: 'id' })).not.toEqual(null);
    expect(spyUsersBizService.findById).toHaveBeenCalled();
    expect(spyUsersBizService.findById).toHaveBeenCalledWith('id');
  });

  it('calling findByEmail user method', () => {
    expect(usersController.findByEmail({ email: 'email' })).not.toEqual(null);
    expect(spyUsersBizService.findByEmail).toHaveBeenCalled();
    expect(spyUsersBizService.findByEmail).toHaveBeenCalledWith('email');
  });

  it('calling delete user method', () => {
    expect(usersController.delete({ id: 'id' })).not.toEqual(null);
    expect(spyUsersBizService.deleteById).toHaveBeenCalled();
    expect(spyUsersBizService.deleteById).toHaveBeenCalledWith('id');
  });

  it('calling create user method', () => {
    const userDTO: UserDTO = {
      email: 'email1@france.fr',
      consent: [],
    };

    expect(usersController.createOrUpdate(userDTO)).not.toEqual(null);
    expect(spyUsersBizService.createOrUpdate).toHaveBeenCalled();
    expect(spyUsersBizService.createOrUpdate).toHaveBeenCalledWith(userDTO);
  });
});
