import { IsEmail, IsNotEmpty, MaxLength, MinLength } from 'class-validator';
import { Consent } from '../../users/interfaces/users.interfaces';

export class EventDTO {
  @IsNotEmpty()
  readonly userId: string;

  @IsNotEmpty()
  @IsEmail()
  @MinLength(1)
  @MaxLength(100)
  readonly userEmail: string;

  @IsNotEmpty()
  readonly userConsent: Consent[];

  @IsNotEmpty()
  readonly creation: Date;
}
