import { Injectable, Logger } from '@nestjs/common';
import mongoose from 'mongoose';
import { EventDTO } from './dto/event.dto';
import { EventsService } from './events.service';
import { Event } from './schemas/event.schema';

@Injectable()
export class EventsBizService {
  private readonly logger = new Logger(EventsBizService.name);

  constructor(private eventsService: EventsService) {}

  public async findAll(): Promise<Event[]> {
    this.logger.debug('[BIZ] Get All events');
    return this.eventsService.findAll();
  }

  public async findByUserEmailEvent(userEmail: string): Promise<Event> {
    this.logger.debug(`[BIZ] Get Event By user email: ${userEmail}`);
    return this.eventsService.findByUserEmailEvent(userEmail);
  }

  public async findById(id: string): Promise<Event> {
    if (!mongoose.Types.ObjectId.isValid(id)) {
      return null;
    }
    this.logger.debug(`[BIZ] Get Event By Id: ${id}`);
    return this.eventsService.findById(id);
  }

  public async create(event: EventDTO): Promise<Event> {
    this.logger.debug(`[BIZ] Create event for user email: ${event.userEmail}`);
    return this.eventsService.create(event);
  }
}
