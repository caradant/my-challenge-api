import { Logger } from '@nestjs/common/services/logger.service';
import { Test, TestingModule } from '@nestjs/testing';
import { EventDTO } from './dto/event.dto';
import { EventsService } from './events.service';

// NOTE: For services in this project, I choose to mock every service and return something expected.
// For me something could be useful here. It's to use "mother classes". To avoid the declaration of many DTO object in each test
// I could export objects from a Mother class and just import them after in test when needed.
class EventsServiceMock {
  findAll() {
    return <EventDTO[]>[
      {
        userId: '1123456789',
        userEmail: 'email@airdot.fr',
        userConsent: [],
        creation: new Date('2022-10-10'),
      },
      {
        userId: '2345678',
        userEmail: 'email2@airdot.fr',
        userConsent: [],
        creation: new Date('2022-10-10'),
      },
    ];
  }
  findById(id: string) {
    Logger.debug(id);
    return <EventDTO>{
      userId: '1123456789',
      userEmail: 'email@airdot.fr',
      userConsent: [],
      creation: new Date('2022-10-10'),
    };
  }
  findByUserEmailEvent(userEmail: string) {
    Logger.debug(userEmail);
    return {
      userId: '1123456789',
      userEmail: 'email@airdot.fr',
      userConsent: [],
      creation: new Date('2022-10-10'),
    };
  }
  create(dto: EventDTO) {
    Logger.debug(dto.userEmail);
    return <EventDTO>{
      userId: '112345678911',
      userEmail: 'email3@airdot.fr',
      userConsent: [],
      creation: new Date('2022-10-10'),
    };
  }
}
describe('EventsService', () => {
  let eventsService: EventsService;

  beforeAll(async () => {
    const ApiServiceProvider = {
      provide: EventsService,
      useClass: EventsServiceMock,
    };
    const module: TestingModule = await Test.createTestingModule({
      providers: [EventsService, ApiServiceProvider],
    }).compile();

    eventsService = module.get<EventsService>(EventsService);
  });

  it('EventsService - should be defined', () => {
    expect(eventsService).toBeDefined();
  });

  it('should call create method with expected params', async () => {
    const expectedEvent: EventDTO = {
      userId: '112345678911',
      userEmail: 'email3@airdot.fr',
      userConsent: [],
      creation: new Date('2022-10-10'),
    };
    const eventCreated = await eventsService.create(expectedEvent);
    expect(eventCreated).toEqual(expectedEvent);
  });

  it('should call findById method with expected param', async () => {
    const eventExpected: EventDTO = {
      userId: '1123456789',
      userEmail: 'email@airdot.fr',
      userConsent: [],
      creation: new Date('2022-10-10'),
    };
    const event = await eventsService.findById('idOfEvent');
    expect(event).toEqual(eventExpected);
  });

  it('should call findByUserEmailEvent method with expected params', async () => {
    const eventExpected: EventDTO = {
      userId: '1123456789',
      userEmail: 'email@airdot.fr',
      userConsent: [],
      creation: new Date('2022-10-10'),
    };
    const eventByUserEmail = await eventsService.findByUserEmailEvent(
      'email@airdot.fr',
    );
    expect(eventByUserEmail).toEqual(eventExpected);
  });

  it('should call findAll method', async () => {
    const eventsExpected = [
      {
        userId: '1123456789',
        userEmail: 'email@airdot.fr',
        userConsent: [],
        creation: new Date('2022-10-10'),
      },
      {
        userId: '2345678',
        userEmail: 'email2@airdot.fr',
        userConsent: [],
        creation: new Date('2022-10-10'),
      },
    ];
    const events = await eventsService.findAll();
    expect(events).toEqual(eventsExpected);
  });
});
