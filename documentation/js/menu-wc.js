'use strict';

customElements.define('compodoc-menu', class extends HTMLElement {
    constructor() {
        super();
        this.isNormalMode = this.getAttribute('mode') === 'normal';
    }

    connectedCallback() {
        this.render(this.isNormalMode);
    }

    render(isNormalMode) {
        let tp = lithtml.html(`
        <nav>
            <ul class="list">
                <li class="title">
                    <a href="index.html" data-type="index-link">challenge-api-2022 documentation</a>
                </li>

                <li class="divider"></li>
                ${ isNormalMode ? `<div id="book-search-input" role="search"><input type="text" placeholder="Type to search"></div>` : '' }
                <li class="chapter">
                    <a data-type="chapter-link" href="index.html"><span class="icon ion-ios-home"></span>Getting started</a>
                    <ul class="links">
                        <li class="link">
                            <a href="overview.html" data-type="chapter-link">
                                <span class="icon ion-ios-keypad"></span>Overview
                            </a>
                        </li>
                        <li class="link">
                            <a href="index.html" data-type="chapter-link">
                                <span class="icon ion-ios-paper"></span>README
                            </a>
                        </li>
                                <li class="link">
                                    <a href="dependencies.html" data-type="chapter-link">
                                        <span class="icon ion-ios-list"></span>Dependencies
                                    </a>
                                </li>
                                <li class="link">
                                    <a href="properties.html" data-type="chapter-link">
                                        <span class="icon ion-ios-apps"></span>Properties
                                    </a>
                                </li>
                    </ul>
                </li>
                    <li class="chapter modules">
                        <a data-type="chapter-link" href="modules.html">
                            <div class="menu-toggler linked" data-toggle="collapse" ${ isNormalMode ?
                                'data-target="#modules-links"' : 'data-target="#xs-modules-links"' }>
                                <span class="icon ion-ios-archive"></span>
                                <span class="link-name">Modules</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                        </a>
                        <ul class="links collapse " ${ isNormalMode ? 'id="modules-links"' : 'id="xs-modules-links"' }>
                            <li class="link">
                                <a href="modules/AppModule.html" data-type="entity-link" >AppModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/AuthModule.html" data-type="entity-link" >AuthModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#controllers-links-module-AuthModule-d5808586d40214bcc444e1d38e5088de78384c18abd8db7130707877726b2a4a4119917c1a44cfea69d3229fc386338c36852a1114fd80bdfb201758ace2999b"' : 'data-target="#xs-controllers-links-module-AuthModule-d5808586d40214bcc444e1d38e5088de78384c18abd8db7130707877726b2a4a4119917c1a44cfea69d3229fc386338c36852a1114fd80bdfb201758ace2999b"' }>
                                            <span class="icon ion-md-swap"></span>
                                            <span>Controllers</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="controllers-links-module-AuthModule-d5808586d40214bcc444e1d38e5088de78384c18abd8db7130707877726b2a4a4119917c1a44cfea69d3229fc386338c36852a1114fd80bdfb201758ace2999b"' :
                                            'id="xs-controllers-links-module-AuthModule-d5808586d40214bcc444e1d38e5088de78384c18abd8db7130707877726b2a4a4119917c1a44cfea69d3229fc386338c36852a1114fd80bdfb201758ace2999b"' }>
                                            <li class="link">
                                                <a href="controllers/AuthController.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >AuthController</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#injectables-links-module-AuthModule-d5808586d40214bcc444e1d38e5088de78384c18abd8db7130707877726b2a4a4119917c1a44cfea69d3229fc386338c36852a1114fd80bdfb201758ace2999b"' : 'data-target="#xs-injectables-links-module-AuthModule-d5808586d40214bcc444e1d38e5088de78384c18abd8db7130707877726b2a4a4119917c1a44cfea69d3229fc386338c36852a1114fd80bdfb201758ace2999b"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-AuthModule-d5808586d40214bcc444e1d38e5088de78384c18abd8db7130707877726b2a4a4119917c1a44cfea69d3229fc386338c36852a1114fd80bdfb201758ace2999b"' :
                                        'id="xs-injectables-links-module-AuthModule-d5808586d40214bcc444e1d38e5088de78384c18abd8db7130707877726b2a4a4119917c1a44cfea69d3229fc386338c36852a1114fd80bdfb201758ace2999b"' }>
                                        <li class="link">
                                            <a href="injectables/AuthService.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >AuthService</a>
                                        </li>
                                        <li class="link">
                                            <a href="injectables/JwtStrategy.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >JwtStrategy</a>
                                        </li>
                                        <li class="link">
                                            <a href="injectables/LocalStrategy.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >LocalStrategy</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                            <li class="link">
                                <a href="modules/CommonModule.html" data-type="entity-link" >CommonModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/EventsModule.html" data-type="entity-link" >EventsModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#controllers-links-module-EventsModule-c70748024bca808b5c1f715ef1e18631247941e105048893108a4856d2ca62b5446b01f288683b7e73482956c66bf2df9cdbb96831af55af45af2acb1ea93ded"' : 'data-target="#xs-controllers-links-module-EventsModule-c70748024bca808b5c1f715ef1e18631247941e105048893108a4856d2ca62b5446b01f288683b7e73482956c66bf2df9cdbb96831af55af45af2acb1ea93ded"' }>
                                            <span class="icon ion-md-swap"></span>
                                            <span>Controllers</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="controllers-links-module-EventsModule-c70748024bca808b5c1f715ef1e18631247941e105048893108a4856d2ca62b5446b01f288683b7e73482956c66bf2df9cdbb96831af55af45af2acb1ea93ded"' :
                                            'id="xs-controllers-links-module-EventsModule-c70748024bca808b5c1f715ef1e18631247941e105048893108a4856d2ca62b5446b01f288683b7e73482956c66bf2df9cdbb96831af55af45af2acb1ea93ded"' }>
                                            <li class="link">
                                                <a href="controllers/EventsController.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >EventsController</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#injectables-links-module-EventsModule-c70748024bca808b5c1f715ef1e18631247941e105048893108a4856d2ca62b5446b01f288683b7e73482956c66bf2df9cdbb96831af55af45af2acb1ea93ded"' : 'data-target="#xs-injectables-links-module-EventsModule-c70748024bca808b5c1f715ef1e18631247941e105048893108a4856d2ca62b5446b01f288683b7e73482956c66bf2df9cdbb96831af55af45af2acb1ea93ded"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-EventsModule-c70748024bca808b5c1f715ef1e18631247941e105048893108a4856d2ca62b5446b01f288683b7e73482956c66bf2df9cdbb96831af55af45af2acb1ea93ded"' :
                                        'id="xs-injectables-links-module-EventsModule-c70748024bca808b5c1f715ef1e18631247941e105048893108a4856d2ca62b5446b01f288683b7e73482956c66bf2df9cdbb96831af55af45af2acb1ea93ded"' }>
                                        <li class="link">
                                            <a href="injectables/EventsBizService.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >EventsBizService</a>
                                        </li>
                                        <li class="link">
                                            <a href="injectables/EventsService.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >EventsService</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                            <li class="link">
                                <a href="modules/UsersModule.html" data-type="entity-link" >UsersModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#controllers-links-module-UsersModule-2603ccc77f92a9a24ff47ad4285066b389fe62ee0f2f2e74bb299f0cc3503ad9dafc93d68a8fd0f832171996d1669a34233ee96bee6170e67420b42e2881721c"' : 'data-target="#xs-controllers-links-module-UsersModule-2603ccc77f92a9a24ff47ad4285066b389fe62ee0f2f2e74bb299f0cc3503ad9dafc93d68a8fd0f832171996d1669a34233ee96bee6170e67420b42e2881721c"' }>
                                            <span class="icon ion-md-swap"></span>
                                            <span>Controllers</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="controllers-links-module-UsersModule-2603ccc77f92a9a24ff47ad4285066b389fe62ee0f2f2e74bb299f0cc3503ad9dafc93d68a8fd0f832171996d1669a34233ee96bee6170e67420b42e2881721c"' :
                                            'id="xs-controllers-links-module-UsersModule-2603ccc77f92a9a24ff47ad4285066b389fe62ee0f2f2e74bb299f0cc3503ad9dafc93d68a8fd0f832171996d1669a34233ee96bee6170e67420b42e2881721c"' }>
                                            <li class="link">
                                                <a href="controllers/UsersController.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >UsersController</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#injectables-links-module-UsersModule-2603ccc77f92a9a24ff47ad4285066b389fe62ee0f2f2e74bb299f0cc3503ad9dafc93d68a8fd0f832171996d1669a34233ee96bee6170e67420b42e2881721c"' : 'data-target="#xs-injectables-links-module-UsersModule-2603ccc77f92a9a24ff47ad4285066b389fe62ee0f2f2e74bb299f0cc3503ad9dafc93d68a8fd0f832171996d1669a34233ee96bee6170e67420b42e2881721c"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-UsersModule-2603ccc77f92a9a24ff47ad4285066b389fe62ee0f2f2e74bb299f0cc3503ad9dafc93d68a8fd0f832171996d1669a34233ee96bee6170e67420b42e2881721c"' :
                                        'id="xs-injectables-links-module-UsersModule-2603ccc77f92a9a24ff47ad4285066b389fe62ee0f2f2e74bb299f0cc3503ad9dafc93d68a8fd0f832171996d1669a34233ee96bee6170e67420b42e2881721c"' }>
                                        <li class="link">
                                            <a href="injectables/UsersBizService.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >UsersBizService</a>
                                        </li>
                                        <li class="link">
                                            <a href="injectables/UsersService.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >UsersService</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                </ul>
                </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#classes-links"' :
                            'data-target="#xs-classes-links"' }>
                            <span class="icon ion-ios-paper"></span>
                            <span>Classes</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="classes-links"' : 'id="xs-classes-links"' }>
                            <li class="link">
                                <a href="classes/Event.html" data-type="entity-link" >Event</a>
                            </li>
                            <li class="link">
                                <a href="classes/EventDTO.html" data-type="entity-link" >EventDTO</a>
                            </li>
                            <li class="link">
                                <a href="classes/ExceptionsFilter.html" data-type="entity-link" >ExceptionsFilter</a>
                            </li>
                            <li class="link">
                                <a href="classes/User.html" data-type="entity-link" >User</a>
                            </li>
                            <li class="link">
                                <a href="classes/UserDTO.html" data-type="entity-link" >UserDTO</a>
                            </li>
                        </ul>
                    </li>
                        <li class="chapter">
                            <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#injectables-links"' :
                                'data-target="#xs-injectables-links"' }>
                                <span class="icon ion-md-arrow-round-down"></span>
                                <span>Injectables</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                            <ul class="links collapse " ${ isNormalMode ? 'id="injectables-links"' : 'id="xs-injectables-links"' }>
                                <li class="link">
                                    <a href="injectables/FakeUsersService.html" data-type="entity-link" >FakeUsersService</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/JwtAuthGuard.html" data-type="entity-link" >JwtAuthGuard</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/JwtStrategy.html" data-type="entity-link" >JwtStrategy</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/LocalAuthGuard.html" data-type="entity-link" >LocalAuthGuard</a>
                                </li>
                                <li class="link">
                                    <a href="injectables/LocalStrategy.html" data-type="entity-link" >LocalStrategy</a>
                                </li>
                            </ul>
                        </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#interfaces-links"' :
                            'data-target="#xs-interfaces-links"' }>
                            <span class="icon ion-md-information-circle-outline"></span>
                            <span>Interfaces</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? ' id="interfaces-links"' : 'id="xs-interfaces-links"' }>
                            <li class="link">
                                <a href="interfaces/Consent.html" data-type="entity-link" >Consent</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/JwtPayload.html" data-type="entity-link" >JwtPayload</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/JwtSign.html" data-type="entity-link" >JwtSign</a>
                            </li>
                            <li class="link">
                                <a href="interfaces/Payload.html" data-type="entity-link" >Payload</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#miscellaneous-links"'
                            : 'data-target="#xs-miscellaneous-links"' }>
                            <span class="icon ion-ios-cube"></span>
                            <span>Miscellaneous</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="miscellaneous-links"' : 'id="xs-miscellaneous-links"' }>
                            <li class="link">
                                <a href="miscellaneous/enumerations.html" data-type="entity-link">Enums</a>
                            </li>
                            <li class="link">
                                <a href="miscellaneous/functions.html" data-type="entity-link">Functions</a>
                            </li>
                            <li class="link">
                                <a href="miscellaneous/typealiases.html" data-type="entity-link">Type aliases</a>
                            </li>
                            <li class="link">
                                <a href="miscellaneous/variables.html" data-type="entity-link">Variables</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <a data-type="chapter-link" href="coverage.html"><span class="icon ion-ios-stats"></span>Documentation coverage</a>
                    </li>
                    <li class="divider"></li>
                    <li class="copyright">
                        Documentation generated using <a href="https://compodoc.app/" target="_blank">
                            <img data-src="images/compodoc-vectorise.png" class="img-responsive" data-type="compodoc-logo">
                        </a>
                    </li>
            </ul>
        </nav>
        `);
        this.innerHTML = tp.strings;
    }
});