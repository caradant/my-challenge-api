import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { ApiProperty } from '@nestjs/swagger';
import { Document } from 'mongoose';
import { Consent } from '../../users/interfaces/users.interfaces';

// TODO: I didn't check constraints between the "Event" Document and "User" Document
// because we don't want dependancies in the case where we remove a user.
// I still want to keep events during X years even if users are still not anymore in DB.

@Schema()
export class Event extends Document {
  @ApiProperty()
  @Prop({ type: String, required: true })
  userId: string;

  @ApiProperty()
  @Prop({ type: String, required: true })
  userEmail: string;

  @ApiProperty({
    type: 'array',
    format: 'object',
    example: '{type: "sms", enabled: true, type: "email", enabled: false}',
  })
  @Prop({ type: Array, required: true })
  userConsent: Consent[];

  @ApiProperty({
    type: 'string',
    format: 'date-time',
    example: '2018-11-21T06:20:32.232Z',
  })
  @Prop({ required: true })
  creation: Date;
}

export const EventSchema = SchemaFactory.createForClass(Event);
