import { ConsentEnum } from '../enums/users.enum';

export interface Consent {
  readonly type: ConsentEnum;
  readonly enabled: boolean;
}
