import { Test, TestingModule } from '@nestjs/testing';
import { CommonModule } from '../common/common.module';
import { FakeUsersService, User } from '../common/services/fake-users.service';
import { AuthController } from './auth.controller';
import { Payload } from './auth.interface';
import { AuthService } from './auth.service';

describe('authController Unit Tests', () => {
  let authController: AuthController;
  let spyAuthService: AuthService;

  beforeAll(async () => {
    // FakeUsersService is injected AuthService
    // For this reason, I simulate a fake provider inside this test component and it doesn't work
    // I even tried to import CommonModule containing the fakeService but it's not working
    const FakeUserServiceProvider = {
      provide: FakeUsersService,
      useFactory: () => ({
        findOne: jest.fn(
          () =>
            <User>{
              username: 'Kevin',
              userId: 1,
              password: 'root',
            },
        ),
      }),
    };
    const AuthServiceProvider = {
      provide: AuthService,
      useFactory: () => ({
        validateUser: jest.fn(
          () =>
            <
              {
                userId: number;
                username: string;
              }
            >{
              username: 'Kevin',
              userId: 1,
            },
        ),
        login: jest.fn(() => ({
          access_token: 'ezrezrezrzerzerezrezrezrez',
        })),
      }),
    };
    const app: TestingModule = await Test.createTestingModule({
      controllers: [AuthController],
      providers: [
        AuthService,
        AuthServiceProvider,
        FakeUsersService,
        FakeUserServiceProvider,
      ],
    }).compile();

    authController = app.get<AuthController>(AuthController);
    spyAuthService = app.get<AuthService>(AuthService);
  });

  it('calling login method', () => {
    const user: Payload = {
      username: 'Kevin',
      userId: '1',
    };

    authController.login(user);
    expect(spyAuthService.login).toHaveBeenCalled();
    expect(spyAuthService.login).toHaveBeenCalledWith(user);
  });
});
