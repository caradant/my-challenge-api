import {
  IsEmail,
  IsNotEmpty,
  IsString,
  MaxLength,
  MinLength,
} from 'class-validator';
import { Consent } from '../interfaces/users.interfaces';

export class UserDTO {
  @IsNotEmpty()
  @IsString()
  @IsEmail()
  @MinLength(1)
  @MaxLength(100)
  readonly email: string;

  @IsNotEmpty()
  readonly consent: Consent[];
}
